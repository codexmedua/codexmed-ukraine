Stack technologique

-   Utilisation principalement de Python comme outil de manipulation de
    données

-   Bases de données PostgreSQL et Elasticsearch envisagée pour backend
    et « search »

-   Application interface proposée avec StreamLit. Interface similaire à
    Google Translate, mais avec des indicateurs de fiabilité et
    plusieurs suggestions.

Sources de données

-   Fédération internationale des pharmaciens : liste anglais-ukrainien
    avec marques commerciales, formats de distribution et référence du
    gouvernement Ukrainien. Document PDF exporté vers Excel
    manuellement. Environ 250 entrées uniques. Clé de fusion :
    Classification ATC

```{=html}
<!-- -->
```
-   Wikidata : Données sur environ 5000 médicaments, surtout en anglais.
    Beaucoup de clés de jointures vers d'autres référentiels et bases de
    données (Classification ATC, Chembl, WHO/UN) . Utilise le web
    sémantique, requêtes avec le langage SPARQL. wikidata.org

-   Source coligée manuellement : Matériel médical (environ 130 entrées)
    anglais-français-ukrainien

Défis

-   Peu de clés communes pour fusionner les sources de données (fusion
    FIP et Wikidata \~ 60% sur code ATC)

-   Données Wikidata sont variables en quantité (beaucoup de valeurs
    manquantes)

-   Peu de sources de références dans le domaine (anglo-centrique)

-   Noms commerciaux vs noms scientifiques

-   Noms peuvent être ambigus (prononciation ou graphie similaire)

<https://nc.jevalide.ca/s/Z45T5f88oBSitrz>

![](media/image1.png){width="6.281248906386701in"
height="7.052082239720035in"}
