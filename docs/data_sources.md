# Data sources

## Priority list

  - [Medical priority list](https://moz.gov.ua/uploads/ckeditor/%D0%9D%D0%BE%D0%B2%D0%B8%D0%BD%D0%B8/Medical-supplies-priority%20needs.xlsx)

## Data sources

  - [GGlossary of health technology assessment. English, French, Russian, German and Spanish](https://htaglossary.net/). Contient [une page web qui serait intéressante de scrapper](https://htaglossary.net/Liste+de+tous+les+termes#A).
  - [Russian version of the Vidal site](https://www.vidal.ru/). Vidal est connu pour sont dictionnaire médical.
  - [Vidal's French list of active substances](https://www.vidal.fr/medicaments/substances/liste-a.html)
  - [EExplanation (wikipedia) of the ATC classification system. Anatomical Therapeutic Chemical](https://fr.wikipedia.org/wiki/Classification_ATC)
  - [Info on WHO ATC/DDD](https://www.who.int/tools/atc-ddd-toolkit/about)
  - [European Union ATC codes](https://ec.europa.eu/health/documents/community-register/html/reg_hum_atc.htm)
  - [SNOMED, medical terminology, contains ontology and software](https://www.snomed.org/)
  - [Merck index, documents the properties of molecules](https://www.rsc.org/merck-index)
  - [Information from CAS number or chemical formula. Chinese(?) and English](https://www.chemsrc.com/en/)
  - [Drug Bank Online, information on drugs. English](https://go.drugbank.com/drugs/DB00316)
  - [Drug Bank online,  vocabulary and structures in csv format](https://go.drugbank.com/releases/latest#open-data)
  - [ProZ. English/ukranian thematic glossaries](https://www.proz.com/glossary-translations/english-to-ukrainian-glossaries)
  - [Information of the International Association of Pharmacists (FIP) for the invasion of Ukraine by Russia](https://www.fip.org/priorityareas-ukraine)
  - [Csv conversion of the translation (English/Ukrainian) of FIP drugs](https://github.com/franc00018/ukrainian_medicine_names_fip_csv)
  - [Dosage conversion tools](https://ukrainemedlist.solutions.iqvia.com/)

## Google Sheets

  - [Valentin's lexicon](https://docs.google.com/spreadsheets/d/1m3LampIajhMrl3ZrUytifBrHyt1YhOvYfSmnX0zH_Gk)
