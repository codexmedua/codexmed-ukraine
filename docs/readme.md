# Codex Medical Prototyping for Ukraine

This repo is part of the [codexmedua project]  (https://gitlab.com/codexmedua). The goal of the repo is to produce a quick prototype of the desired features, to experiment and document them for a more robust implementation.

## Why a codex?

The codex is the early form of the modern book that replaced paper rolls. At the time (around the 5th century) it was an important invention, especially because this format made it easier to access information in any part of a document.

In a similar way, the codexmedua project aims to make the information needed by health professionals in crisis situations easily available.

## Background and issues

Medical assistance in crisis situations has several particularities, including the following

  - urgent and rapid action
  - need to coordinate efforts
  - the need for technical translation

Concerning the codex, there are 3 types of operations:

  - Finding and importing reliable information
  - Store in a structured and usable form
  - Provide relevant information to the people who need it

