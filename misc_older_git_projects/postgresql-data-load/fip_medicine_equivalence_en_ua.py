import pandas as pd
import psycopg2_conn
import copy_from_stringio as cfs
import psycopg2_schema_if_not_exists as ps_schema
import psycopg2_table_if_not_exists as ps_table

source = "https://github.com/franc00018/ukrainian_medicine_names_fip_csv/raw/main/Equivalence%20between%20medicines%20registered%20in%20Ukraine%20and%20other%20countries_clean.csv"

raw_data = pd.read_csv(source)

conn = psycopg2_conn.connect()

ps_schema.schema_if_not_exists(conn=conn,
                               schema="medecine")

raw_data.columns = ['atc_classification',
                    'activeingredient_en',
                    'activeingredient_ua_cyrillic',
                    'brand_ua_cyrillic',
                    'brand_ua_latin',
                    'manufacturer',
                    'dosage_form_au_cyrillic',
                    'dosage_form_en',
                    'information']

raw_data2 = raw_data.replace(r'\n', ';', regex=True)

ps_table.table_if_not_exists(conn=conn,
                             table="medecine.fip_medicine_equivalence_en_ua",
                             create_statement="""
                             id integer not null,
                             atc_classification text,
                             activeingredient_en text,
                             activeingredient_ua_cyrillic text,
                             brand_ua_cyrillic text,
                             brand_ua_latin text,
                             manufacturer text,
                             dosage_form_au_cyrillic text,
                             dosage_form_en text,
                             information_ua text
                             """)

conn

cfs.copy_from_stringio(conn=conn,
                       df=raw_data2,
                       table="medecine.fip_medicine_equivalence_en_ua")
