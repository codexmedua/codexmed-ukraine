def table_if_not_exists(conn,table,create_statement):
    """Creates a table if it doesn t exist

    Args:
        conn (Connection): psycopg2 database connection
        table (string): Table name
        create_statement (string): DDL of created table
    """
    try:
        cur = conn.cursor()
        cur.execute("""
        DROP TABLE IF EXISTS {table};
        """.format(table=table))
        cur.execute("""
        CREATE TABLE IF NOT EXISTS {table} ({create_statement});
        """.format(table=table,create_statement=create_statement))
    except Exception as e:
        print("Erreur lors de la création de la table: \n {e}".format(e=e))
        pass
    finally:
        cur.close()
        conn.commit()