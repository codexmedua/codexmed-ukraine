import psycopg2


def connect():
    """connect to postgresql

    Returns:
        Connection: new database connection to postgresql
    """
    return psycopg2.connect(
        database="codexmedua",
        user="codexmedua",
        password="",
        host="localhost",
        port="5432")
